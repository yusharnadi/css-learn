const menuToggle = document.querySelector('.menu-toggle');
const nav = document.querySelector('nav ul');

menuToggle.addEventListener('click', () => {
    visibility = nav.dataset.visible;

    if (visibility === 'false') {
        nav.setAttribute('data-visible', true);
        menuToggle.setAttribute('aria-expended', true);
        menuToggle.querySelector('span').innerText = 'close';
    } else if (visibility === 'true') {
        nav.setAttribute('data-visible', false);
        menuToggle.setAttribute('aria-expended', false);
        menuToggle.querySelector('span').innerText = 'menu';
    }
});
